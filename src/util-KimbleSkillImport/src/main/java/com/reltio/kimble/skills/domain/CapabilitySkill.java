package com.reltio.kimble.skills.domain;

public class CapabilitySkill {
	private String capabilityType;
	
	public CapabilitySkill(String capabilityType) {
		this.capabilityType = capabilityType;
	}

	public String getCapabilityType() {
		return capabilityType;
	}

	public void setCapabilityType(String capabilityType) {
		this.capabilityType = capabilityType;
	}
}
