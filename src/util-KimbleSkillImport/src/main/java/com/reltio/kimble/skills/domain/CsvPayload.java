package com.reltio.kimble.skills.domain;

import java.util.LinkedHashMap;

public class CsvPayload {
	private String id;
	private String resource;
	private String crosswalkKey;
	private String capabilityGroup;
	private String capabilityType;
	private String grade;
	private String resourceType;
	
	@SuppressWarnings("rawtypes")
	public CsvPayload(LinkedHashMap map) {
		if(map.get("id") != null)
			this.id = "" + map.get("id");
		
		if(map.get("resource") != null)
			this.resource = "" + map.get("resource");
		
		if(map.get("crosswalkKey") != null)
			this.crosswalkKey = "" + map.get("crosswalkKey");
		
		if(map.get("capabilityGroup") != null)
			this.capabilityGroup = "" + map.get("capabilityGroup");
			
		if(map.get("capabilityType") != null)
			this.capabilityType = "" + map.get("capabilityType");
		
		if(map.get("grade") != null)
			this.grade = "" + map.get("grade");
		
		if(map.get("resourceType") != null)
			this.resourceType = "" + map.get("resourceType");
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getResource() {
		return resource;
	}
	public void setResource(String resource) {
		this.resource = resource;
	}
	public String getCrosswalkKey() {
		return crosswalkKey;
	}
	public void setCrosswalkKey(String crosswalkKey) {
		this.crosswalkKey = crosswalkKey;
	}
	public String getCapabilityGroup() {
		return capabilityGroup;
	}
	public void setCapabilityGroup(String capabilityGroup) {
		this.capabilityGroup = capabilityGroup;
	}
	public String getCapabilityType() {
		return capabilityType;
	}
	public void setCapabilityType(String capabilityType) {
		this.capabilityType = capabilityType;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getResourceType() {
		return resourceType;
	}
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}
	
	public String toString() {
		return "{id=" + getId() + "}," +
				"{resource=" + getResource() + "}," +
				"{crosswalkKey=" + getCrosswalkKey() + "}," +
				"{capabilityGroup=" + getCapabilityGroup() + "}," +
				"{capabilityType=" + getCapabilityType() + "}," +
				"{grade=" + getGrade() + "}," +
				"{resourceType=" + getResourceType() + "}";
	}
}
