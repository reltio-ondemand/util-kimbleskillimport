package com.reltio.kimble.skills.domain;

import java.util.LinkedHashMap;

public class ReltioResponse {
	private String successful;
	
	@SuppressWarnings("rawtypes")
	public ReltioResponse(LinkedHashMap map) {
		if(map.get("successful") != null)
			this.successful = ""+ map.get("successful");
	}

	public String getSuccessful() {
		return successful;
	}

	public void setSuccessful(String successful) {
		this.successful = successful;
	}
	
	public String toString() {
		return "{successful=" + successful + "}";
	}
}
