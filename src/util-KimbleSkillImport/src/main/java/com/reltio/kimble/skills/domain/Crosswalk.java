package com.reltio.kimble.skills.domain;

import java.util.List;

public class Crosswalk {
	private String crosswalkKey;
	private List<CapabilityGroup> capabilityGroupList;
	
	public Crosswalk(String crosswalkKey) {
		this.crosswalkKey = crosswalkKey;
	}
	
	public String getCrosswalkKey() {
		return crosswalkKey;
	}
	public void setCrosswalkKey(String crosswalkKey) {
		this.crosswalkKey = crosswalkKey;
	}
	public List<CapabilityGroup> getCapabilityGroupList() {
		return capabilityGroupList;
	}
	public void setCapabilityGroupList(List<CapabilityGroup> capabilityGroupList) {
		this.capabilityGroupList = capabilityGroupList;
	}
}
