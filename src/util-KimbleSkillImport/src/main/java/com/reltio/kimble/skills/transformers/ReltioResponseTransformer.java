package com.reltio.kimble.skills.transformers;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import com.reltio.kimble.skills.domain.ReltioResponse;

public class ReltioResponseTransformer extends AbstractMessageTransformer{
    /**
     * @param args
     */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
    
		List<LinkedHashMap> reltioResponseList = (List<LinkedHashMap>) message.getPayload();
		
		List<LinkedHashMap> returnList = new ArrayList<LinkedHashMap>();
		LinkedHashMap<String, String> returnMap = new LinkedHashMap<String, String>();
		
		int successCount = 0;
		int failCount = 0;
		
		if(reltioResponseList.size() > 0) {
			for(LinkedHashMap map : reltioResponseList) {
				ReltioResponse reltioResponse = new ReltioResponse(map);
				if(reltioResponse.getSuccessful().equals("true")) {
					successCount++;
				} else {
					failCount++;
				}
			}
			
			returnMap.put("message", "Kimble Skill import complete.");
			
		} else {
			returnMap.put("message", "Kimble Skill import did not complete.");
		}
		
		returnMap.put("profiles successfully updated", "" + successCount);
		returnMap.put("profiles failed update", "" + failCount);
		
		returnList.add(returnMap);
		
    	return returnList;
    }
}