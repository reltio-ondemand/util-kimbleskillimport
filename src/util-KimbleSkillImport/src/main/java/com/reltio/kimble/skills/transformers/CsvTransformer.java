package com.reltio.kimble.skills.transformers;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import com.reltio.kimble.skills.domain.CapabilityGroup;
import com.reltio.kimble.skills.domain.CapabilitySkill;
import com.reltio.kimble.skills.domain.Crosswalk;
import com.reltio.kimble.skills.domain.CsvPayload;

public class CsvTransformer extends AbstractMessageTransformer{
    /**
     * @param args
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
    	List<Crosswalk> crosswalkList = new ArrayList<Crosswalk>();
    	
    	try {
    		Map<String, Map<String, List<CapabilitySkill>>> crosswalkMap = new HashMap<String, Map<String, List<CapabilitySkill>>>();
			
			List<LinkedHashMap> csvPayloadList = (List<LinkedHashMap>) message.getPayload();
			
			for(LinkedHashMap mapElement : csvPayloadList) {
				CsvPayload payload = new CsvPayload(mapElement);
				
				if(payload.getCrosswalkKey() != null && !payload.getCrosswalkKey().isEmpty()) {
					if(crosswalkMap.get(payload.getCrosswalkKey()) == null){
						Map<String, List<CapabilitySkill>> capabilityGroupMap = new HashMap<String, List<CapabilitySkill>>();
						List<CapabilitySkill> capabilityList = new ArrayList<CapabilitySkill>();
						CapabilitySkill capabilitySkill = new CapabilitySkill(payload.getCapabilityType());
						
						capabilityList.add(capabilitySkill);
						capabilityGroupMap.put(payload.getCapabilityGroup(), capabilityList);
						
						crosswalkMap.put(payload.getCrosswalkKey(), capabilityGroupMap);
					} else {
						Map<String, List<CapabilitySkill>> capabilityGroupMap = crosswalkMap.get(payload.getCrosswalkKey());
						
						if(capabilityGroupMap.get(payload.getCapabilityGroup()) == null){
							List<CapabilitySkill> capabilityList = new ArrayList<CapabilitySkill>();
							CapabilitySkill capabilitySkill = new CapabilitySkill(payload.getCapabilityType());
									
							capabilityList.add(capabilitySkill);
							
							capabilityGroupMap.put(payload.getCapabilityGroup(), capabilityList);
						} else {
							List<CapabilitySkill> capabilityList = capabilityGroupMap.get(payload.getCapabilityGroup());
							CapabilitySkill capabilitySkill = new CapabilitySkill(payload.getCapabilityType());
							
							capabilityList.add(capabilitySkill);
							capabilityGroupMap.put(payload.getCapabilityGroup(), capabilityList);
						}
						
						crosswalkMap.put(payload.getCrosswalkKey(), capabilityGroupMap);
					}
				}
			}
			
			for (Map.Entry<String, Map<String, List<CapabilitySkill>>> crosswalkMapEntry : crosswalkMap.entrySet()) {
				String crosswalkMapKey = crosswalkMapEntry.getKey();
				Map<String, List<CapabilitySkill>> capabilityGroupMap = crosswalkMapEntry.getValue();
				
				List<CapabilityGroup> capabilityGroupList = new ArrayList<CapabilityGroup>();
				
				for (Map.Entry<String, List<CapabilitySkill>> capabilityGroupMapEntry : capabilityGroupMap.entrySet()) {
					String capabilityGroupMapKey = capabilityGroupMapEntry.getKey();
					List<CapabilitySkill> capabilityGroupSkillList = capabilityGroupMapEntry.getValue();
					
					CapabilityGroup capabilityGroup = new CapabilityGroup(capabilityGroupMapKey);
					capabilityGroup.setCapabilityGroupSkillList(capabilityGroupSkillList);
					capabilityGroupList.add(capabilityGroup);
				}
				
				Crosswalk crosswalk = new Crosswalk(crosswalkMapKey);
				crosswalk.setCapabilityGroupList(capabilityGroupList);
				crosswalkList.add(crosswalk);
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
        return crosswalkList;
    }
}