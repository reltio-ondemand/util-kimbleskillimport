package com.reltio.kimble.skills.domain;

import java.util.List;

public class CapabilityGroup {
	private String capabilityGroup;
	private List<CapabilitySkill> capabilityGroupSkillList;
	
	public CapabilityGroup(String capabilityGroup) {
		this.capabilityGroup = capabilityGroup;
	}
	
	public String getCapabilityGroup() {
		return capabilityGroup;
	}
	public void setCapabilityGroup(String capabilityGroup) {
		this.capabilityGroup = capabilityGroup;
	}
	public List<CapabilitySkill> getCapabilityGroupSkillList() {
		return capabilityGroupSkillList;
	}
	public void setCapabilityGroupSkillList(List<CapabilitySkill> capabilityGroupSkillList) {
		this.capabilityGroupSkillList = capabilityGroupSkillList;
	}
	
	
}
